FROM python:3.7-alpine3.7
ENV PYTHONUNBUFFERED 1
RUN set -e;

RUN apk add --update --no-cache \
    python3 \
    python3-dev \
    build-base \
    linux-headers \
    pcre-dev \
    py-pip \
    curl \
    openssl

ADD requirements/main.txt /app/requirements/main.txt
RUN pip install -r /app/requirements/main.txt --no-cache-dir

ADD . /app/
WORKDIR /app/

CMD ["uwsgi", "--ini", "uwsgi.ini"]